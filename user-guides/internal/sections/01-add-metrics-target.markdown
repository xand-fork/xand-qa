# Collecting Metrics

TPFS maintains a central metrics server that will gather metrics from all of the network
participants. These instructions explain how to include a new participant to be scraped by our 
central server.

## Get Participant Metrics Server Information

We provide each participant the ability to run a Prometheus metrics server on their cluster and
to expose that server to be scraped by TPFS. In order to gain access to their metric data, we 
must receive from the partner:

* Partner Name
* Metrics Server Domain Name
* Valid JWT (JSON Web Token) for Authorization

## Store Participant Metrics Server Information

Ensure you have access to Vault.

We store all of the participant's metrics server information in Vault. From there, our central 
server deployment has access to Vault can add the participant to be scraped.

The path for production metrics targets in Vault is: `secret/environments/prod/monitoring/targets/`

To add a new participant you will add a new key to this path named for the participant. That key 
must include the data fields:

* `tag`: Unique company name in **snake_case**
* `prom_url`: Metrics Server Domain Name. Do not include `https://` or trailing `/`
* `jwt`: Valid JWT

To add a new key, set the environment variables:

```bash
COMPANY_NAME= <Company name in snake_case>
DOMAIN_NAME= <Metrics Server Domain Name>
JWT= <Valid JWT>
```

**WARNING:** Ensure that the `COMPANY_NAME` is unique, otherwise it will overwrite the existing key.
You can ensure that a key doesn't already exist with the same name by attempting to get it from Vault

```bash
vault kv get secret/environments/prod/monitoring/targets/$COMPANY_NAME
```

you should get the message:

```
No value found at secret/data/environments/prod/monitoring/targets/<Your COMPANY_NAME>
```

Then run

```bash
vault kv put secret/environments/prod/monitoring/targets/$COMPANY_NAME \ 
        prom_url=$DOMAIN_NAME jwt=$JWT tag=$COMPANY_NAME
```

Verify that the key has been added properly with

```bash
vault kv get secret/environments/prod/monitoring/targets/$COMPANY_NAME
```
It should look something like

```
====== Metadata ======
Key              Value
---              -----
created_time     2020-08-21T22:53:05.119927048Z
deletion_time    n/a
destroyed        false
version          3

====== Data ======
Key         Value
---         -----
jwt         eyJhbGdiOiJHUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteWNvbXBhhjkiLCJhdWQiOiJ0cGZzIiwiZXhwIjoxNjI5NDE3OTE4LCJpYXQiOjE1ORc4ODE5MTh9.LURuhzTBE1G2FZJkn_1y5ePjVe5X7NLuOtBKfvXX3Hw
prom_url    valid.domain.name
tag         acme_inc
```

## Update Deployment in Octopus

Ensure you have access to Octopus.

TPFS manages the central metrics server deployment using Octopus. The deployment script for the 
server has access to Vault, from which it can get a list of all the targets and build a 
Prometheus Config that includes those targets for metrics aggregation.

We use the same script for deploying the central server as we use for updating the server. 

TODO: Include more detail once production server is running

To include the metrics target we just added to Vault, find the production central server
deployment within the Central Metrics Server project. Click on the deployment that has been deployed
to the production cluster, and from the drop-down next to `DEPLOY TO...` click `Re-Deploy...` and 
then `DEPLOY` again.

![octopus redeploy](images/octo_redeploy.png)

This will take about 1 minute as it allows Prometheus time to recognize the new configuration.

If the deployment fails, ensure that the data in Vault is correctly formatted.

## Verify Participant's Metrics Are Being Scraped

The easiest way to verify the participant's metrics are being scraped is through the 
Prometheus Web UI. You can view the UI either by

* either, hitting the central server directly from your browser
* or, port-forwarding the pod using `kubectl` to go around authorization

### Kubectl Port-Forward Method

In order to port forward using `kubectl` you will need to ensure your context is set to the 
production cluster. 

TODO: Include instructions for accessing actual cluster

Set the pod name:
```bash
POD_NAME=$(kubectl get pods -n monitoring -l app="prometheus-server" -o name)
```

And forward the port to access the Prometheus server via localhost:

```bash
kubectl port-forward -n monitoring $POD_NAME 9090
```

The Prometheus UI should now be available at `http://localhost:9090`

### Verify

From the UI, from the top drop-down click Status -> Targets (In the port-forward case this would be
`http://localhost:9090/targets`). Ensure that the participant is included as 
`federate-<COMPANY_NAME>` and that the State is `UP`
