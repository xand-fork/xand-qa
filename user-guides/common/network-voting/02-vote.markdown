## Voting

To vote on a proposal use either
```bash
network-voting-cli vote accept <proposal_id>
```
to accept the proposal, or
```bash
network-voting-cli vote reject <proposal_id>
```
to reject the proposal
