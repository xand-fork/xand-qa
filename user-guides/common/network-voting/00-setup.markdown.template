# Participating in XAND Network Voting

## Pre-Requisites
You'll need:

- Your own Public Key (Authority Address for Validator)
- The URL of the XAND network API

## Introduction

Control of the XAND Network is decentralized. This means no one organization has the ability to add
or remove participants, change network settings, or update the blockchain source code. Instead,
all participating members and validators form a council that make these decisions.

Each participant has the ability to propose administrative actions to the council and each participant
has the ability to vote on those proposals. A limited agent - an entity appointed by the members of
a Xand network - can submit proposals, but not vote. Once the required number of participants have voted in favor
of a proposed action, that action will automatically be enacted.

In order for a proposal to pass, 2/3 of the voting power must accept the proposal, where 50% of the
voting power is controlled by the members collectively and 50% of the voting power is controlled
by the validators collectively.

>For example: If there are 3 members and 5 validators and all 3 members vote in favor of a proposal,
>that means 1/2 of the voting power has accepted and another 1/6 of the voting power is required for
>it to pass (1/2 + 1/6 = 2/3). Thus, at least 2 of the 5 validators (1/5 of total voting power is
>greater than 1/6) must approve the proposal for it to pass.

Inversely, if more than 1/3 of the voting power votes to reject the proposal, it will automatically
be removed.

All proposals will be visible until they expire, at which point they will be removed. Proposals that
have been accepted will also stay visible until the expiration, but will be marked as `Accepted`.

## Setup

In order to participate in the network voting, use the Network Voting CLI tool.

> Note: The Network Voting CLI tool is not packaged with the `xand-devops-toolchain` docker image.
> If you have been working out of the image, be sure to exit and work from your host machine.

$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for network voting CLI Setup**
</span>

<span style="color: darkred;">
Instead of aliasing the network voting CLI from our external docker repository you'll need to use the internal version on gcr:
```bash
alias network-voting-cli="docker run -it --rm -v /tmp/.network:/root/.config/xand gcr.io/xand-dev/network-voting-cli:0.13.0"
```
</span>

---

$endif$

Alias the tool as `network-voting-cli`:
```bash
alias network-voting-cli="docker run -it --rm -v /tmp/.network:/root/.config/xand transparentinc-docker-external.jfrog.io/network-voting-cli:0.12.0"
```

The default output will be the help menu, which can also be accessed with the `-h` flag:
```bash
network-voting-cli -h
```

Follow any command or subcommand with the `-h` flag to get specific instructions for that action.

Participating with the cli will require you to provide the url of the network api, the participant's
public key, and optionally the jwt provided by the network api. When setting the -u option to
 specify the URL, ensure the URL supplied is prefixed with "https://".  To set these
 config values:
```bash
network-voting-cli set-config -u <api_url> -i <participant_public_key>
```
There is also an optional JWT `-j` field that can be set in the config.

To check these values use:
```bash
network-voting-cli get-config
```
