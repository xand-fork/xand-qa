# Accessing the Trust API

In your browser of choice, navigate to the root path of your trust-api server (e.g. https://trust-api.mydomain.com/). You should see a graphical representation of all the routes provided with your trust-api.
