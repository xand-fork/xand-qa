$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Instructions**
</span>

<span style="color: darkred;">
This version of the document is intended to be used in Simulation Game Days (SGD). This document is not intended to be
used on a real Xand Network Deployment. **Please Reach out to support@tpfs.io if you are deploying a real Xand Network Deployment and seeing this message**
</span>

---

<span style="color: darkred;">
**SGD Overall Notes**
</span>

<span style="color: darkred;">
There are a few reasons for why we have different instructions for our SGDs compared to our customer facing installation.
Here are some of them that you might want to be aware of:
</span>

* Sharing a single domain for all participants of the SGD.
  * Saves us time from having to manage a lot of domains, and simpler to track down if there's an issue.
* SGD uses pre-released software compared to real deployments which should only have access to formally released software.
* Setting up and maintaining Cloud Provider Accounts is time consuming
  * This is similar to domains where tracking down issues and maintenance is simpler/faster.
  * The other possible alternative is sending everyone a company credit card and waiting for the accounts to turn on, and then having to shutdown the account on completion. (AWS may not let you configure an account after you've shutdown an existing account)
* We want to capture log data and metric data differently then a real network. The data is useful for the day, but won't be after that day.

<span style="color: darkred;">
Other house keeping:
</span>

* Remember that there's an hour and a half designated for finishing your initial setup
  * It's okay if takes longer for you. Just make sure that the Network Coordinators are aware of your progress if it does. We will vote you in later.
  * We appreciate your time and it's great if you're running into issues since that's what we're hoping to find before our customers run into them.
  * We just want to be cognisant of everyone's time, and it's why this rule exists.

---

<span style="color: darkred;">
**SGD Example Notes**
</span>

<span style="color: darkred;">
You will see notes like this before sections to give you context about differences that will be occurring during an SGD. These should be the only differences between an SGD and a real deployment. So please read the notes as well as the normal sections to verify what customers will see in a real deployment as well as to know how you'll be testing differently during an SGD.
</span>

---

$endif$
# Introduction

Welcome to our Xand Network Deployment Guide for $partner_type_prefix$ $partner_type$!
This guide uses an application called Ansible which will handle automating your Xand Network Deployment.
This document will walk you through your entire Xand Network Deployment which will consist of the following overall steps:

* Preliminary setup of Docker, Python and Ansible
* Configure variables supplied by Transparent Systems
* Configure variables for your organization.
* Deploy your Xand Network by running an Ansible playbook
* Share your Xand Network Deployment Information to join the Xand Network
* If you're an initial participant in the launch of a Xand Network
  * Transparent Financial Systems will coordinate with you and the other participants for the launch of a new network.
  * You will be asked to re-run the deployment which will update your deployment to network with the other participants.
* Use Network Voting CLI to confirm access/deployment on the network and an introduction to how to vote.
$if(member)$
* Configure Bank Account Information to create and redeem claims
$endif$

# What you'll need

* A computer that will be used through this process in which your private keys will be created on.
  * At the end of the install. You'll find all of your secret and deployment files located at `$host_save_dir$`.
* An Account at a Cloud Provider (AWS, Azure, Google Cloud)
  * The Region/Zone where you would like to have the network deployed.
  * Ability to create credentials that have full access to the Cloud Provider account.
  * There will be a new Virtual Private Cloud (VPC) or Virtual Network (VN) that should create separation from other deployed content in an existing account.
* A Top Level Domain (e.g. mydomain.com) where you can create new DNS Records for.
  * Xand API domain: "xand-api.mydomain.com"
$if(member)$
  * Member API domain: "member-api.mydomain.com"
$endif$
$if(trust)$
  * Trust API domain: "trust-api.mydomain.com"
$endif$
  * Xand Metrics endpoint to confirm status of your node if sharing metrics data: "prometheus.mydomain.com"
  * A url to view your Xand Metrics with this grafana domain: "monitoring.mydomain.com"
$if(fill_out_bank_credentials)$
  * Bank Credentials and Urls for bank accounts that you will be connecting to.
    * Keep in mind that you may have to allowlist the IP from this process with said Bank before you're able to make requests to the Bank.
$endif$

$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for Preliminary Setup**
</span>

<span style="color: darkred;">
In this next section, instead of contacting <support@tpfs.io> as a customer would normally when issues arise. Please use Troubleshooting Etiquette:
</span>

* Check the corresponding #sgd-MM-dd-yyyy slack channel for pinned solutions that could already solve the issue that you're experiencing.
* Otherwise bring it up in the Zoom Video Conference with one of the Network Coordinators that might be available.
  * If a conversation with a Network Coordinator or other member of the team lasts longer than 5 minutes. Please use break out rooms to minimize cross chatter on the zoom call.
  * If both Network Coordinators are occupied. Send a message on slack to see if another participant has worked through a similar issue or might be available to troubleshoot.

---

<span style="color: darkred;">
The Artifactory Credentials and Loggly Token mentioned below can be found in the `Credentials` tab of the excel sheet. For the Loggly Tags follow these steps.
</span>

* Go to the `Ansible Vars` tab of the excel sheet.
* Find your `Role` by finding your name in the `Operator` column.
* Now would be a good time to fill out the "{FILL_ME_OUT}" in the `Company Name` column.
* You'll find your Loggly Tags in column `N` which will look something like the following "sgd-yyyy-MM-dd,val0-MM-dd".

---

$endif$
# Preliminary Setup
Reach out to <support@tpfs.io> for the following.

- Artifactory username and password
- Loggly Token and Loggly Tags

## Install Docker

Make sure to have installed the following:

- [Docker](https://docs.docker.com/get-docker/)

Check that you have `docker` installed correctly by running
```bash
docker run -it --rm hello-world
```

> On Linux, if you receive a _Permission denied_ error, it's likely that your user does not have access
> to issue docker commands. See [these instructions](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
> to add your `$$USER` to the docker group.

## Install Python v3.6+
Python 3.6+ is required. Check that you have the required version:
```bash
python3 --version
```

If Python3 is not installed, or your version does not meet the minimum requirements, then please update according to your OS below:

### MacOS (Big Sur & Catalina)
Python3 does not come built-in to MacOS, but does come with Apple's separate Command Line Tools (CLT) package.

To install the CLT. Open a terminal and run:
```bash
xcode-select --install
```

If the Command Line Tools are already installed, but Python3 is out of date, then update the CLT via the following steps:
```
Click: Control Panel -> Software Update -> (optionally: More info...) -> Select "Command Line Tools" -> Install Now.
```

### Windows 10
We recommend running Ubuntu 20.04 under Windows Subsystem for Linux v2 (WSL2). Here are instructions for running WSL2 and Ubuntu: https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps

### Ubuntu 20.10 (Groovy)  & 20.04 (Focal) & 18.04 (bionic)

Ubuntu 20.10, 20.04, and 18.04 come with the required Python3 version, but not Pip3. Use APT to install Pip3, and then update pip3 via python3.n

```bash
sudo apt update
sudo apt install python3-pip
sudo python3 -m pip install --upgrade pip
```

### CentOS 7

Use epel-release and install python 3.6 from that repo:
```bash
sudo yum install epel-release 
sudo yum install python36 
```

### Ubuntu 16.04 (Xenial), Debian 9 (Stretch)

You will need to compile Python yourself on these platforms. Download/Compile instructions:
```bash
sudo apt update
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev -y
curl -O https://www.python.org/ftp/python/3.6.13/Python-3.6.13.tar.xz
tar -xf Python-3.6.13.tar.xz
cd Python-3.6.13
./configure --enable-optimizations
make
sudo make install
sudo /usr/local/bin/python3 -m pip install --upgrade pip
```

Then restart your terminal.

### Any other linux platform where your package manager will not update to 3.6+

You may need to compile Python yourself on these platforms.

Packages that are needed (look up based on your platform):

- build-essential 
- zlib1g-dev 
- libncurses5-dev 
- libgdbm-dev 
- libnss3-dev 
- libssl-dev 
- libsqlite3-dev 
- libreadline-dev 
- libffi-dev 
- curl 
- libbz2-dev 


Download/Compile instructions:
```bash
curl -O https://www.python.org/ftp/python/3.6.13/Python-3.6.13.tar.xz
tar -xf Python-3.6.13.tar.xz
cd Python-3.6.13
./configure --enable-optimizations
make
sudo make install
sudo /usr/local/bin/python3 -m pip install --upgrade pip
```

Then restart your terminal.
