$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for Google Cloud**
</span>

<span style="color: darkred;">
The `Ansible Vars` tab in the excel sheet has been filled out to help make the process of filling out this part of the document easier. 
Please go to row `19` which will have some different values that we're looking for you to fill out. The order doesn't match this file, and it's because
of the differences that exist in all the different cloud providers. The mapping below will help distinguish the different files.
</span>

* project_id = column `C` `Project/Subscription Id`
* region = column `D` `Region`
* zone = column `E` `Zone`
* gcp_t4_bucket = column `G` `Bucket/Storage Account`
* cluster_name = column `F` `Cluster Name`
* service_account_src = Save Contents of notes to a file named something like `~/xand_qa_service_account.json` and enter `~/xand_qa_service_account.json` as the value here.

**DO NOT run ./scripts/create_gcloud_service_account.sh**  
You can just skip that step below.

---

$endif$
# (Google Cloud) Fill out the gke.yml file

If you're deploying to Google Cloud, use the following instructions:

## Service Account

When deploying to gcloud, a service user must be created with owner role permissions to the appropriate project. For convenience, a script has been included in the `/scripts` folder called `create_gcloud_service_account.sh` that will create such a service user. When run, it will generate a credentials json file for this service user in the ansible `/vars` folder, and then generate or update a file called `gke.yml` in the Ansible `/vars` folder with the path to this json credentials file.
 
```bash
./scripts/create_gcloud_service_account.sh
```

The following information in `vars/gke.yml` will be automatically filled out by this script. If you are using the script, please skip ahead to the Additional Variables section. If you are [creating the service account yourself](https://cloud.google.com/iam/docs/creating-managing-service-accounts), you’ll need to fill out the variables in this section by hand.

service_account_src: The path to the Service Account credentials json file that was generated when you created a service account.

```yaml
  service_account_src: "/path/to/json/creds.json"
```

## Additional Variables

The following variables need to be filled out before running a playbook.

project_id: The project id for the Google Cloud project you want to deploy to.

```yaml
  project_id: "MyProject"
```

region: The Google Cloud region you wish to deploy to. (Choose whatever is closest to your end users)

```yaml
  region: "us-west-1"
```

zone: The Google Cloud zone you wish to deploy to. (Choose whatever is closest to your end users)

```yaml
  zone: "us-west1-a"
```

gcp_t4_bucketname: Choose a name for the Cloud Storage Bucket that will be created for storing the terraform state.

```yaml
  gcp_t4_bucketname: "MyStorageBucket"
```

cluster_name: Choose a cluster name for your GKE kubernetes cluster.

```yaml
  cluster_name: "MyKubernetesCluster"
```

### Deployment Variables

The following variables included in the template file can be left as-is -- they will be filled in automatically during
deployment:

* `resource_name_prefix`
* `min_node_count`
* `max_node_count`
