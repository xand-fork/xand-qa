
# (AWS) Fill out the eks.yml file

$if(show_sgd_notes)$
---

<span style="color: darkred;">
**SGD Notes for AWS**
</span>

<span style="color: darkred;">
Unlike other cloud providers you will be using your own user credentials to deploy AWS resources. 
Please reach out to a Network Coordinator if you need credentials or need your password reset.
To obtain your IAM access key, login to AWS console with your account (Should be [firstname].[lastname]) and generate a new Access Key for your account.
If you do not have an account or can't access it, ask for help from an engineer that already has AWS access.  They can create your account so you can login.
</span>

<span style="color: darkred;">
The `Ansible Vars` tab in the excel sheet has been filled out to help make the process of filling out this part of the document easier. 
Please go to row `19` which will have some different values that we're looking for you to fill out. The order doesn't match this file, and it's because
of the differences that exist in all the different cloud providers. The mapping below will help distinguish the different files.
</span>

* region = column `D` `Region`
* cluster_name = column `F` `Cluster Name`
* s3_bucket = column `G` `Bucket/Storage Account`
* access_key_id = Located within your IAM profile for Access Keys
* secret_access_key = Located within your IAM profile for Access Keys

---

$endif$

If you're using AWS, use the following instructions:

## IAM Access Key

An IAM user with Owner permission's Access key ID and Secret access key are required to deploy to AWS. You will need to include them in the eks.yml file before deployment. If you do not currently have an Access key ID and Secret access key, [you will need to generate one using the AWS console.](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey)

access_key_id: The ID of the AWS IAM Access key that will be used to deploy the infrastructure.

```yaml
  access_key_id: "XXXXXXXXXXXXXXXXXXXX"
```

secret_access_key: The secret access key that was generated when you created your AWS IAM access key.

```yaml
  secret_access_key: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

## Additional Variables

The following variables need to be filled out before running a playbook.

region: The AWS region you wish to deploy to. (Choose whatever is closest to your end users)

```yaml
  region: "us-west-2"
```

cluster_name: Choose a cluster name for your EKS kubernetes cluster.

```yaml
  cluster_name: "MyCluster"
```

s3_bucket: Choose a name for the s3 bucket that will be created for storing the terraform state.

```yaml
  s3_bucket: "MyTerraformStateBucket"
```

### Deployment Variables

The following variables included in the template file can be left as-is -- they will be filled in automatically during
deployment:

* `resource_name_prefix`
* `scaling_min_size`
* `scaling_max_size`
