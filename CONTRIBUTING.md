[[_TOC_]]

## Tagging

This repo uses Git [tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 
to track manual tests of the instructions found in the user guides.

Considerations:
* **Version**: We version using the date of the related Simulation Game Day (SGD) in which a user guide was tested. The reason for this is that we are not currently versioning the user guides. 
* **Bug-fixed docs**: If desiring to tag such docs that were updated as a direct result of the testing event (e.g. patch, bug-fix), append the tag with `-post`, e.g. `v.05232020-post`
* **Commit version**: It is recommended to tie our tags to commit SHAs for ease of cross-reference. 

Components of a tag:
* SHA:  `<Git commit SHA representing the version of the docs tested>`, e.g `babb4f1c53f775c8b63412760df6008e328eb932`
* Tag: `v.<yyyy-mm-dd>`, e.g `v.2020-05-23` 
* Message: `Description of activity`, e.g. `Mini SGD`


### See all tags

```bash
git fetch --tags
git log --tags --simplify-by-decoration --pretty="format:%ai %d %p %N"

2020-08-03 18:38:09 +0000  (tag: v.2020-07-24-post) fac5810 
2020-07-23 18:27:26 +0000  (tag: v.2020-07-24) dddfd83 
```

Or more simply:

```bash
git fetch --tags
git tag -l --sort=-creatordate 

v.2020-07-24-post
v.2020-07-24
```

### Add a tag

Add tag(s) after the commit has been merged to master. 

Create a tag locally
```bash
# git tag -a <tag> -m "<message>" <sha>
git tag -a v.2020-05-13 -m "Description of a testing event" babb4f1c53f775c8b63412760df6008e328eb932
```

Verify it has been created
```bash
git tag -l

v.2020-05-13
```

Push tag(s)
```bash
$ git push origin --tags
```
